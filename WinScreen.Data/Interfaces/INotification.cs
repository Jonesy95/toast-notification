﻿using System;

namespace WinScreen.Data
{
	public interface INotification
	{
		void SendNotification(string message);
	}
}

