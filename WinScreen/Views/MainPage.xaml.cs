﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace WinScreen
{
	public partial class MainPage : ContentPage
	{
		public MainPage ()
		{
			var imageWin = new Image
			{
				Source = "imageWin.png",
				HorizontalOptions = LayoutOptions.Center,
			};

			var imagePrize = new Image 
			{
				Source = "imagePrize.png",
				WidthRequest = 600,
				HorizontalOptions = LayoutOptions.Center,
			};

			var buttonContinue = new Button
			{
				//Image = "imageContinue.png",
				WidthRequest = 600,
				HeightRequest = 100,
				Text = "Continue",
				FontSize = 60,
				BackgroundColor = Color.Green,
				HorizontalOptions = LayoutOptions.Center
			};

			Content = new StackLayout {
				Spacing = 80,
				BackgroundColor = Color.Black,
				//BackgroundColor = Color.Green,
				Orientation = StackOrientation.Vertical,
				HorizontalOptions = LayoutOptions.Center,
				Children = { imageWin, imagePrize, buttonContinue }
			};
			InitializeComponent ();
		}
	}
}

