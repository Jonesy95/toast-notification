﻿using System;
using Android.App;
using Android.Content;

namespace WinScreen.Droid
{
	public class MyAndroidNotification : Android.Support.V4.App.Fragment
	{
		private NotificationManager notificationManager;

		public void SendNotification(String message)
		{
			notificationManager = (NotificationManager)Activity.GetSystemService (Context.NotificationService);

			var builder = new Notification.Builder (Activity)
				.SetPriority ((int)NotificationPriority.Default)
				.SetCategory (Notification.CategoryMessage)
				.SetContentTitle ("Woolette")
				.SetContentText (message).Build ();

			notificationManager.Notify (1, builder);
		}
	}
}

