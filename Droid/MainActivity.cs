﻿using System;

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Microsoft.Practices.ServiceLocation;
using GalaSoft.MvvmLight.Ioc;
using WinScreen.Data;

namespace WinScreen.Droid
{
	[Activity (Label = "WinScreen.Droid", Icon = "@drawable/icon", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
	public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsApplicationActivity
	{
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			global::Xamarin.Forms.Forms.Init (this, bundle);
			SimpleIoc.Default.Register<INotification> (MyNotification,true); 

			LoadApplication (new App ());

		}
	}
}

